module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({

    // watch for changes
    watch: {
      files: ['js/src/*.js', 'css/*.scss','css/modules/*.scss','html-src/*.html','html-src/partials/*.html'],
      tasks: ['default'],
      options: {
        livereload: true
      }
    },

    // sass compile task
    sass: {
      dist:{
        options:{
          style : 'compressed'
        },
        files:{
          'css/main.css' : 'css/main.scss'
        }
      }
    },

    // minify css
    cssmin: {
      minify: {
        src: ['css/main.css'],
        dest: 'css/main.min.css'
      }
    },

    // concat
    concat: {
      options: { separator: ';\n' },
      dist: {
        src: [
          'js/src/vendor/modernizr-custom.min.js',
          'js/src/vendor/classList.polyfill.js',
          'js/src/vendor/string.contains.polyfill.js',
          'bower_components/jquery/dist/jquery.min.js',
          'js/src/vendor/jquery.fancybox.js',
          'js/src/vendor/jquery.fancybox-media.js',
          'bower_components/jquery-mousewheel/jquery.mousewheel.min.js',
          'bower_components/hammerjs/hammer.js',
          'bower_components/swipejs/swipe.js',
          'bower_components/history.js/scripts/bundled/html4+html5/jquery.history.js',
          'js/src/luz.js'
        ],
        dest: 'js/dist/luz.js'
      }
    },

    // uglify js
    uglify: {
      dist: {
        files: {
          'js/dist/luz.min.js': ['js/dist/luz.js']
        }
      }
    },

    // build html
    bake:{
      dist:{
        options :{},
        files:{
          'index.html'          : 'html-src/index.html',
          'list.html'           : 'html-src/list.html',
          'author.html'         : 'html-src/author.html',
          'authors.html'        : 'html-src/authors.html',
          'single.html'         : 'html-src/single.html',
          'single_1el.html'     : 'html-src/single_1el.html',
          'insight.html'        : 'html-src/insight.html',
          'contact.html'        : 'html-src/contact.html',
          'feature.html'        : 'html-src/feature.html',
          'feature_video.html'  : 'html-src/feature_video.html',
          'clients.html'        : 'html-src/clients.html'
        }
      }
    },

    // server
    connect : {
      server :{
        options : {
          port      : 9000,
          hostname  : '0.0.0.0',
          livereload: 35729
        },
        livereload: {
          options: {
            open: true,
            base: [
              '.tmp',
              './'
            ]
          }
        }
      }
    },

    // svg
    svgstore: {
      options: {
        'prefix' : 'icn-',
        'includedemo' : true
      },
      default : {
        files: {
          'html-src/partials/icons.html': ['img/*.svg']
        }
      }
    }

  });

  grunt.registerTask('default',['bake','sass','cssmin','concat','uglify']);
  grunt.registerTask('server',['connect','watch']);

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-bake' );
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-svgstore');

};