/* global Modernizr*/

(function(w,d){
  'use strict';

  /**
   * Image lazy loaded class
   *
   * @class ImageLazy
   * @constructor
   */

  function ImageLazy(el,caller,fn){
    this.self   = $(el);
    this.caller = caller;
    this.fn     = fn;
    this.image  = this.self.find('>img');
    this.cover  = this.self.find('.img-lazyload_img');
    this.loader = this.self.find('.img-lazyload_loader');
    this.src    = this.image.attr('data-src');

    // extra args
    if( arguments.length > 3 ){
      this.args = Array.prototype.slice.call(arguments,3);
    }

    var self = this;

    this.image.on('load',function(){ self.loaded();});

    window.setTimeout(function(){
      self.startLoading();
    },1000);
  }

  ImageLazy.prototype.startLoading = function(){
    this.image.attr('src',this.src);
  };

  ImageLazy.prototype.loaded = function(){
    var self = this;
    // check if is a fake load event caused from cache
    if( this.image[0].getAttribute('src').contains('data:image') ){
      this.image.on('load',function(){ self.loaded();});
      return;
    }

    this.self.addClass('img-loaded');

    if( this.image[0].naturalHeight && this.image[0].naturalHeight >= this.image[0].naturalWidth ){
        this.self.addClass('media-vertical');
    }

    if(this.self.hasClass('img-cover')){
      this.cover.css({
        'background' : 'url('+this.src+') no-repeat',
        'background-position' : 'center center',
        'background-size' : 'cover'
      });
    }
    if(this.caller && this.args) {
      this.fn.apply(this.caller,this.args);
    }
    if(this.caller && !this.args){
      this.fn.call(this.caller);
    }
  };



  /**
   * Frame images obj for video media
   *
   * @class VideoFrames
   * @constructor
   */
  function VideoFrames(obj){
    this.self     = $(obj);
    this.frames   = this.self.find('picture');
    this.index    = 0;
    this.duration = 800;
    this.items    = this.frames.length;

    this.start();
  }

  VideoFrames.prototype.start = function(){
    var self = this;
    setTimeout(function(){
      var next = (self.index<self.items-1) ? self.index+1 : 0;
      self.frames[self.index].style.opacity = 0;
      self.frames[next].style.opacity = 1;
      self.index = next;
      next = null;
      self.start();
    },this.duration);
  };



  /**
   * Homepage slider
   *
   * @class HpSlider
   * @constructor
   */

  function HpSlider(){
    this.self         = $('.hpslider');
    this.slider       = this.self.find('.hpslider_content');
    this.items        = this.self.find('.hpslider_el');
    this.tags         = this.self.find('.hpslider_tags span');
    this.callout      = this.self.find('.hpslider_callout');
    this.calloutLink  = this.callout.find('.hpslider_action');
    this.projects     = this.self.find('.hpslider_project span');
    this.prev         = this.self.find('.hpslider_prev');
    this.next         = this.self.find('.hpslider_next');
    this.count        = this.items.length;
    this.index        = 0;
    this.timer        = null;
    this.delay        = 5000;
    this.duration     = 500;
    this.loaded       = false;
    this.images       = [];
    this.imagesLoaded = 0;
    this.block        = false;
    this.currentOverlay = 0;

    app.resize(this);

    this.setup();
    this.buildNavigator();

    var self  = this,
        $body = $('body');

    // lazy load images
    this.self.find('.img-lazyload').each(function(i,el){
      self.images.push(new ImageLazy(el,self,self.isLoaded));
    });

    // block autoscroll
    this.self.find('.hpslider_callout,.hpslider_overlay').hover(
      function(){ self.block = true;},
      function(){self.block = false;}
    );


    if( !Modernizr.touch ){
      this.callout.find('h3 a').hover(function(){
        self.items[self.index].style.opacity    = 0.1;
        self.projects[self.index].style.opacity = 1;
      },function(){
        self.items[self.index].style.opacity    = 1;
        self.projects[self.index].style.opacity = 0;
      });
    }

    // bind on mousewheel
    $body.on('mousewheel', function (evt, delta, deltaX, deltaY) {
      var direction;

      if( evt.target.classList.contains('hpslider_info') ){
        evt.preventDefault();
      }else{
        // prevent hpslider animation when the scroll event happens on another element
        return;
      }

      if( !self.block && !app.blockScroll ){
        self.block = true;
        direction = ( deltaY > 0 ) ? -1 : 1;
        self.to(self.index + direction,function(){
          self.block = false;
        });
      }
    });

    // bind events on arrows
    this.prev.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.to(self.index-1);
    });
    this.next.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.to(self.index+1);
    });

    if( Modernizr.touch ) {
      this.bindMobileEvents();
    }

    // bind on custom event in order to block scroll
    app.$w.on('nav-open',function(){
      self.block = true;
    });
    app.$w.on('nav-close',function(){
      self.block = false;
    });

  }

  HpSlider.prototype.updateUI = function(){
    this.prev.css('opacity', ((this.index>0)? 1 : 0) );
    this.next.css('opacity', ((this.index<(this.items.length-1))? 1 : 0) );
  };

  HpSlider.prototype.isLoaded = function(){
    this.imagesLoaded += 1;
    if(this.imagesLoaded === this.images.length-1){

      var self = this;

      app.$w.trigger('pageLoadingEnd');

      // set slideshow ready
      window.setTimeout(function(){
        self.self.addClass('hpslider__ready');
        self.tags.eq(0).addClass('hpslider_tags__show');
        // clean up for GC
        this.images = null;
      },1000);


      this.self.find('.hpslider_el__video').each(function(){
        new VideoFrames(this);
      });

    }
  };

  HpSlider.prototype.bindMobileEvents = function(){
    var self = this;

    d.addEventListener('touchmove',function(evt){
     evt.preventDefault();
    });

    var hammertime = new Hammer(this.self[0]);
    hammertime.on('swipeup',function(){
      if( self.block ){
        return;
      }

      self.block = true;
      self.to(self.index+1,function(){
        self.block = false;
      });
    });
    hammertime.on('swipedown',function(){

      if( self.block ){
        return;
      }

      self.block = true;
      self.to(self.index-1,function(){
        self.block = false;
      });
    });
  };

  HpSlider.prototype.startTimer = function(){
    var self = this;
    setTimeout(function(){
      if( !self.block ){
        self.next();
      }
      self.startTimer();
    },this.delay);
  };

  HpSlider.prototype.setup = function(){
    this.animationProp  = ( Modernizr.csstransforms3d ) ? app.prefixedProperty('transform') : 'top';
    this.animationValue = ( Modernizr.csstransforms3d ) ? 'translate3d(0px,__VALUE__px,0px)' : '__VALUE__px';
    this.slideSize      = this.self.find('.hpslider_wrapper').height();
    this.slider.css('height',this.count*this.slideSize);
    this.items.height(this.slideSize);
    this.calloutLink.attr('href',this.projects[0].getAttribute('data-href'));
    this.updateUI();

  };

  HpSlider.prototype.buildNavigator = function(){
    var html  = '<ul class="hpslider_nav">',
        self  = this;
    html +=Array.prototype.reduce.call(this.items,function(acc,i,index){ acc +='<li>'+index+'</li>'; return acc; },'');
    html +='</ul>';
    this.navigator          = $(html).appendTo(this.self);
    this.navigatorElements  = this.navigator.find('li');

    this.navigator.css( 'margin-top' , (-1)*(this.items.length*(10+12)) );

    this.navigatorElements.eq(0).addClass('hpslider_nav-current');

    this.navigatorElements.on('click',function(){
      var index = parseInt(this.textContent);
      self.to(index);
    });

  };

  HpSlider.prototype.to = function(i,fn){

    // prevent extra slide
    if( i >= this.count || i<0){
      this.block = false;
      return;
    }

    var animationValue = this.animationValue.replace('__VALUE__',((-1)*this.slideSize*i).toString());
    this.slider.css( this.animationProp , animationValue );
    this.navigatorElements.removeClass('hpslider_nav-current');
    this.navigatorElements.eq(i).addClass('hpslider_nav-current');

    this.tags.removeClass('hpslider_tags__show');
    this.tags.eq(i).addClass('hpslider_tags__show');

    this.calloutLink.attr('href',this.projects[i].getAttribute('data-href'));

    this.index = i;

    this.updateUI();

    if(fn){
      setTimeout(function() {
        fn();
      },this.duration*2);
    }


  };
  HpSlider.prototype.prev = function(){};
  HpSlider.prototype.next = function(){
    var i = (this.index < this.count-1) ? this.index+1 : 0;
    this.to(i);
  };

  HpSlider.prototype.update = function(){
    this.setup();
  };



  /**
   * Navigation module
   *
   * @class Navigation
   * @constructor
   */
  function Navigation(){
    // append extra navigation
    var c     = $('.nav-wrapper').clone(),
        self  = this;
    c.addClass('nav-mobile');
    this.wrapper  = $('#navigation');
    this.self     = c.appendTo($('body'));
    this.toggler  = $('.nav-toggle');
    this.content  = $('#content');

    setTimeout(function(){
      self.toggler.css('display','block');
    },2000);

    //bind events
    $('.nav-trigger').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.show();
    });
    $('.nav-close').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.hide();
    });

    this.toggler.on(app.startEvent,function(evt){
      evt.preventDefault();
      app.$w.trigger('nav-toggleIn');
    });

    app.$w.on('nav-toggleIn',function(){self.toggleIn();});
    app.$w.on('nav-toggleOut',function(){self.toggleOut();});
  }

  Navigation.prototype.show = function(){
    app.$w.trigger('nav-open');
    this.self.addClass('nav-mobile__show');
  };
  Navigation.prototype.hide = function(){
    app.$w.trigger('nav-close');
    this.self.removeClass('nav-mobile__show');
  };

  Navigation.prototype.toggleIn = function(){
    this.content.removeClass('nav-toggleOut').addClass('nav-toggleIn');
  };
  Navigation.prototype.toggleOut = function(){
    this.content.removeClass('nav-toggleIn').addClass('nav-toggleOut');
  };



  /**
   * Filters module
   *
   * @class Filters
   * @constructor
   */
  function Filters(){
    this.overlay = $('.filters-overlay');
    this.self    = $('.filters');
    this.trigger = $('.filters-trigger');
    this.close   = $('.filters-close');


    // setup
    this.update();
    app.resize(this);

    var self = this;
    // bind events
    this.trigger.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.showFilters();
    });
    this.close.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.hideFilters();
    });
  }

  Filters.prototype.setup = function(){};
  Filters.prototype.showFilters = function(){
    this.overlay.addClass('filters-overlay__visible fadeIn');
    this.self.addClass('filters__visible fadeInUp');
    this.trigger.addClass('fadeOut');

    if( Modernizr.touch ){
      $(d).on('touchmove',function(evt){
        if( evt.target.classList.contains('filters-overlay') ) {
          evt.preventDefault();
        }
      });
    }

  };
  Filters.prototype.hideFilters = function(){
    var self = this;
    this.overlay.addClass('fadeOut');
    this.self.addClass('fadeOutDown');
    this.trigger.removeClass('fadeOut').addClass('fadeIn');

    if( Modernizr.touch ){
      $(d).on('touchmove').off();
    }

    // clean up animation classes
    setTimeout(function(){
      self.overlay.removeClass('filters-overlay__visible fadeIn fadeOut');
      self.self.removeClass('filters__visible overlay__visible fadeInUp fadeOutDown');
    },1100);
  };
  Filters.prototype.update = function(){
    var coords = this.trigger.offset();
    this.close.css('top',coords.top+14);
  };


  /**
   * OverableElements elements
   *
   * @class Slider
   * @param {string} selector
   * @param {string} blockClass
   * @constructor
   */
  function OverableElements(selector,blockClass){
    this.items  = $(selector);
    this.time   = 300;

    var self = this;

    if( Modernizr.touch ){

      this.items.each(function() {
        var el      = $(this),
            overlay = el.find('.overlay');

          el.on('click', function () {

          /*evt.preventDefault();
          evt.stopImmediatePropagation();*/

          if (this.classList.contains(blockClass)) {
            return;
          }

          // show overlay
          overlay.stop().fadeIn(self.time);
        });

        el.find('.overlay-close').on(app.startEvent, function (evt) {
          evt.preventDefault();
          evt.stopImmediatePropagation();
          overlay.stop().fadeOut();

        });
      });
    }else{
      this.items.each(function(){
        var el      = $(this),
            overlay = el.find('.overlay'),
            self    = this;

        el.hover(function(evt){
          if( evt.target.tagName === 'A' ){ return; }

          // do not show overlay if the element contains the block class
          if( this.classList.contains(blockClass) ){
            return;
          }
          overlay.stop().fadeIn(self.time);
        },function(){
          overlay.stop().fadeOut(self.time);
        });

      });
    }
  }

  /**
   * Horizontal slideshow
   *
   * @class Slider
   * @constructor
   */
  function Slider(options){
    this.self               = $('.slider');
    this.wrap               = this.self.find('.slider-wrap');
    this.items              = this.self.find('.slider-el');
    this.count              = this.items.length;
    this.prev               = this.self.find('.slider-prev');
    this.next               = this.self.find('.slider-next');
    this.index              = 0;
    this.loaded             = 0;
    this.loadCounter        = this.self.find('.slider-counter');
    this.images             = [];
    this.width              = 0;
    this.offset             = 0;
    this.animation          = { property : '' , value: '' };
    this.options            = options || { transitionStart : null , transitionEnd: null, toggleNav : true, othersOverlay : true };
    this.overlays           = this.self.find('.overlay');
    this.firstLoadElements  = Math.min( (~~(w.innerWidth/381)+2) , this.items.length );
    this.fullWidth          = false;
    this.animationQueue     = 0;
    this.init();
  }


  Slider.prototype.loadImages = function(i,k){
    var self          = this,
        loadNowImages,
        img;

    loadNowImages = this.images.slice(i,k+1);
    loadNowImages.each(function(index){
      self.images[index] = null;
      if( this ){
        img = new ImageLazy(this,self,self.imageLoaded,this.parentElement);
      }

    });

  };

  Slider.prototype.slideAtStartup = function(){

    if(w.innerWidth <= 768 ){
      return;
    }

    var params  = window.location.search.substring(1).split('&'),
      len       = params.length,
      self      = this,
      param,
      toValue   = -1;
    for( var i = 0; i<len; i++ ){
      param = params[i].split('=');
      if( param[0] === 'i' ){
        toValue = parseInt(param[1]);
        break;
      }
    }

    if( toValue > 0 && toValue < this.count ){
      app.$w.trigger('nav-toggleOut');
      setTimeout(function(){
        self.to( toValue );
      },1000);
    }
  };

  Slider.prototype.init = function(){
    var self = this;

    // load first batch of images
   this.images = this.self.find('.img-lazyload');

   if( w.innerWidth <= 768 ){
     this.loadImages(0,this.images.length);
   }else{
     this.loadImages(0,this.firstLoadElements);
   }


    // bind events on controls
    this.prev.on(app.startEvent,function(evt){
      evt.preventDefault();

      if(!self.fullWidth && self.options.toggleNav ){
        app.$w.trigger('nav-toggleOut');
        return;
      }

      self.to(self.nextPageIndex());

    });
    this.next.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.to(self.prevPageIndex());
    });

    // bind events on navigation
    app.$w.on('nav-toggleOut',function(){
      app.slider.resize(true);
    });
    app.$w.on('nav-toggleIn',function(){
      app.slider.resize(false);
    });

    this.overable = new OverableElements('.slider-el','slider-el__inactive');

    // setup for animation
    if( Modernizr.csstransforms3d ){
      this.animation.property = app.prefixedProperty('transform');
      this.animation.value    = 'translate3d(__VALUE__px,0px,0px)';
    }else{
      this.animation.property = 'left';
      this.animation.value    = '__VALUE__';
    }

    // register on window resize event
    app.resize(this);

  };



  Slider.prototype.to = function(i){
    var self = this;
    // prevent extra slides
    if(i<0){
      return;
    }

    this.animationQueue+=1;

    this.reactiveAllElements();

    var offset = 0;
    for(var j=0;j<i;j++) {
      offset += this.items[j].clientWidth+2;
    }
    this.offset = (-1)*(offset+2);

    this.loadImages(i,i+this.firstLoadElements);

    this.index = i;

    this.updateControls();
    // transition start callback
    if( this.options.transitionStart ){
      this.options.transitionStart(this.index);
    }
    this.wrap.css(this.animation.property,this.animation.value.replace('__VALUE__',this.offset.toString()));


    // transition end callback
    setTimeout(function(){
      if( self.options.transitionEnd ) {
        self.options.transitionEnd(self.index);
      }
      self.animationQueue -= 1;

      if( self.animationQueue === 0 ){
        //self.updateInactiveElements();
      }

    },600);


  };

  Slider.prototype.update = function(){
    //this.updateInactiveElements();
  };


  Slider.prototype.updateCounter = function(n){
    var l    = this.images.length,
        text = ( n >= l ) ? l : n+'/'+l;
    this.loadCounter.text( text );
  };

  Slider.prototype.imageLoaded = function(element){
    this.loaded += 1;
    setTimeout(function(){
      element.classList.add('slider-el__loaded');
    },1000);

    if( this.loaded === this.firstLoadElements ){
      this.self.addClass('slider-loaded');
      this.updateControls();
      //this.updateInactiveElements();
      app.$w.trigger('pageLoadingEnd');
      this.slideAtStartup();
    }

  };

  Slider.prototype.resize = function(flag){
    if( flag ){
      this.self.addClass('slider-fullwidth');
      this.fullWidth = true;
    }else{
      this.self.removeClass('slider-fullwidth');
      this.fullWidth = false;
    }
  };

  Slider.prototype.updateControls = function(){

    if( this.index === 0 && (this.wrap.width()+this.offset)<this.self.width() ){
      this.next.addClass('slider-control__disabled');
      this.prev.addClass('slider-control__disabled');
      return;
    }

    if( this.index === 0 ){
      this.next.addClass('slider-control__disabled');
      this.prev.removeClass('slider-control__disabled');
      return;
    }
    if( (this.wrap.width()+this.offset)<this.self.width() ){
      this.prev.addClass('slider-control__disabled');
      this.next.removeClass('slider-control__disabled');
      return;
    }

    this.next.removeClass('slider-control__disabled');
    this.prev.removeClass('slider-control__disabled');

  };

  Slider.prototype.nextPageIndex = function(){
    var k         = this.index,
        l         = this.items.length,
        winWidth  = w.innerWidth; // header width

    while( k<l ){
      var boundRect = this.items[k].getBoundingClientRect();
      if( (boundRect.width+boundRect.left) > winWidth ){
        break;
      }
      k++;
    }

    return k;

  };

  Slider.prototype.prevPageIndex = function(){
    var k         = this.index,
        offset    = 0,
        winWidth  = w.innerWidth,
        bounds;

    while( k>0 ){
      bounds = this.items[k].getBoundingClientRect();
      offset += bounds.width;

      if( offset > winWidth ){
        break;
      }

      k--;
    }

    return k;

  };

  Slider.prototype.reactiveAllElements = function(){
    this.items.removeClass('slider-el__inactive');
  };

  Slider.prototype.updateInactiveElements = function(){
    var k         = this.index,
        l         = this.items.length,
        winWidth  = w.innerWidth; // header width

    // do not updates when it's not needed
    if( !this.options.othersOverlay ){
      return;
    }

    // prevent updates on small viewport
    if( winWidth <= 768 ){
      return;
    }

    while( k<l ){
      var boundRect = this.items[k].getBoundingClientRect();
      if( (boundRect.width+boundRect.left) > winWidth ){
        break;
      }
      k++;
    }

    if( this.items[k] ){
      this.items[k].classList.add('slider-el__inactive');
    }

  };



  /**
   * Author page
   *
   * @class Author
   * @constructor
   */

  function Author(){
    this.self                 = $('.author-profile');
    this.thumbs               = this.self.find('.author-projects_thumbs .img-lazyload ');
    this.padding              = this.self.find('.author-projects_single_padding');
    this.projects             = [];
    this.images               = [];
    this.imageCounter         = 0;
    this.index                = -1;
    this.showcases            = {};
    this.mobileOffset         = this.self.find('.author-projects_offset');
    this.lightbox            = new Lightbox();

    this.loadThumbs();

    this.mobileOffset.css('height',this.self.find('.author-info').height());
    this.fetchShowcases();
    this.setUpThumbs();

    app.resize(this);

    // build projects map and
    // bind events
    var self    = this,
        items  = this.self.find('.author-projects_thumbs li');

    items.each(function(index){
      var obj = $(this),i;
      obj.attr('data-project-index',index);
      self.projects.push(
        {
          href  : obj.attr('data-project-full'),
          title : obj.attr('data-project-title')
        }
      );


      obj.on(app.startEvent,function(){

        // re-order projects
        var projectsOrdered = [],
            current         = this.getAttribute('data-project-index');

        for(i=current;i<4;i++){
          projectsOrdered.push(self.projects[i]);
        }
        for(i=0; i<current; i++){
          projectsOrdered.push(self.projects[i]);
        }

        self.lightbox.show(projectsOrdered);


      });

    });

    this.self.find('.author-skills a').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.loadShowcase(this.getAttribute('data-showcase-id'));
    });


  }

  Author.prototype.loadThumbs = function(){
    var self = this;
    this.thumbs.each(function(i,e){
      self.images.push( new ImageLazy(e,self,self.imagesLoaded) );
    });
  };

  Author.prototype.setUpThumbs = function(){

    if( window.innerWidth > 768 ){
      var cellH = (window.innerHeight-1)/2;
      for(var i=0;i<this.thumbs.length;i++){
        this.thumbs[i].style.height = cellH+'px';
      }
    }

    if( window.innerWidth <= 768 ){
      for(var j=0;j<this.thumbs.length;j++){
        this.thumbs[j].style.height = 'auto';
      }
    }

  };

  Author.prototype.imagesLoaded = function(){
    this.imageCounter += 1;
    // setup thumbs and add ready class on large viewports
    if( this.imageCounter === this.thumbs.length ){
      app.$w.trigger('pageLoadingEnd');
    }

  };

  Author.prototype.fetchShowcases = function(){
    var self = this;
    this.self.find('.author-showcase').each(function(){
      var links = this.children,
          obj   = [],
          id    = this.getAttribute('data-showcase-id');
      for(var i=0; i<links.length;i++){
        obj.push({ href: links[i].getAttribute('href') , title:links[i].getAttribute('data-showcase-title') });
      }
      self.showcases[id] = obj;
    });
  };

  Author.prototype.update = function(){
    this.setUpThumbs();
  };


  Author.prototype.loadShowcase = function(id){
    this.lightbox.show(this.showcases[id]);
  };

  /**
   * General page loader
   *
   * @class Loader
   * @constructor
   */

  function Loader(){
    var self = this;
    this.loaderoverlay = d.getElementById('luz-loader');
    d.body.classList.add('page-loading');

    app.$w.on('pageLoadingEnd',function(){
      self.hide();
    });

  }

  Loader.prototype.hide = function(){
    var self = this;
    this.loaderoverlay.classList.add('fadeOut');
    setTimeout(function(){
      d.body.classList.remove('page-loading');
      self.loaderoverlay.parentNode.removeChild(self.loaderoverlay);
    },500);
  };

  /**
   * Custom google maps marker with label
   *
   * @class AuthorMapMarker
   * @constructor
   */
  function AuthorMapMarker(id,map,marker,caller,fnClick){
    this._id        = id;
    this.fn         = fnClick;
    this.caller     = caller;
    this._marker    = marker;
    this._iconOff   = (Modernizr.touch) ? app.resURL+'img/author-marker_off__mobile.png'  : app.resURL+'img/author-marker_off.png';
    this._iconOn    = (Modernizr.touch) ? app.resURL+'img/author-marker_on__mobile.png'   : app.resURL+'img/author-marker_on.png';
    this.offset     = (Modernizr.touch) ? 16 : 8;
    this._marker.setIcon(this._iconOff);
    this._event     = (Modernizr.touch) ? 'mouseover' : 'click';
    this._size      = (Modernizr.touch) ? 16 : 8;

    this._marker.setMap(map);
    this.setMap(map);
  }

  // prevent unreferenced error on other pages
  if( window.google ) {
    AuthorMapMarker.prototype = new google.maps.OverlayView();
  }

  AuthorMapMarker.prototype.draw = function(){};
  AuthorMapMarker.prototype.onAdd = function(){
    var panes = this.getPanes(),
        self  = this;

    // append event div
    this.eventPane            = d.createElement('div');
    this.eventPane.className  = 'author-map_marker';

    this.eventPane.style.width  = this._size+'px';
    this.eventPane.style.height = this._size+'px';
    panes.overlayMouseTarget.appendChild(this.eventPane);

    // bind listeners
    google.maps.event.addDomListener(this.eventPane,'mouseover',function(){
      if( self.caller.infoboxVisible ){
        return;
      }
      self.setOn();
    });
    google.maps.event.addDomListener(this.eventPane,'mouseout',function(){
      if( !self.caller.infoboxVisible ){
        self.setOff();
      }
    });

    /*google.maps.event.addDomListener(this.eventPane,this._event,function(evt){
      evt.preventDefault();
      evt.stopPropagation();
      var coords = self.getProjection().fromLatLngToContainerPixel(self._marker.getPosition());
      self.fn.apply(self.caller,[self._id,coords,self]);
    });*/

    google.maps.event.addListener(this._marker,this._event,function(){
      var coords = self.getProjection().fromLatLngToContainerPixel(self._marker.getPosition());
      self.fn.apply(self.caller,[self._id,coords,self]);
    });


    this.setPosition();

    google.maps.event.addListener(this._marker.getMap(), 'zoom_changed',function(){
      self.setPosition();
    });
    google.maps.event.addListener(this._marker.getMap(), 'center_changed',function(){
      self.setPosition();
    });
    google.maps.event.addListener(this._marker.getMap(), 'bounds_changed',function(){
      self.setPosition();
    });

  };

  AuthorMapMarker.prototype.setPosition = function(yOffset){
    var position          = this.getProjection().fromLatLngToDivPixel(this._marker.getPosition());
    if (typeof yOffset === 'undefined') {
      yOffset = 0;
    }
    this.eventPane.style.left = (Math.round(position.x)-this.offset)+'px';
    this.eventPane.style.top = (Math.round(position.y)-this.offset)+'px';
  };

  AuthorMapMarker.prototype.setOff = function(){
    this._marker.setIcon(this._iconOff);
  };
  AuthorMapMarker.prototype.setOn  = function(){
    this._marker.setIcon(this._iconOn);
  };

  /**
   * Authors custom map
   *
   * @class AuthorsMap
   * @constructor
   */

  function AuthorsMap(){
    this.self           = $('.authors-map');
    this.map            = {}; // google map obj
    this.canvas         = this.self.find('.authors-map_canvas');
    this.infobox        = this.self.find('.authors-map_infobox');
    this.infoboxContent = this.infobox.find('.authors-map_infobox_content');
    this.infoboxTitle   = this.infobox.find('.authors-map_infobox_city h6');
    this.slider         = $('.slider__authors');
    this.sliderContent  = this.slider.find('.slider-wrap');
    this.content        = $('#content');
    this.overables      = new OverableElements('.slider-el','authors-filters__disabled');
    this.infoboxVisible = false;
    this.currentMarker  = null;
    this.panelOverlay   = $('.authors-map_overlay');
    this.panelVisible   = false;

    // map custom style
    this.mapStyle   = [
      {'stylers'    :[{'visibility':'simplified'}]},
      {'stylers'    :[{'color':'#131314'}]},
      {'featureType':'water','stylers':[{'color':'#131313'},{'lightness':7}]},
      {'elementType':'labels.text.fill','stylers':[{'visibility':'on'},{'lightness':25}]},
      {'elementType':'labels','stylers': [{ 'visibility': 'off' }]}
    ];
    this.mapOptions = {
      center            : new google.maps.LatLng(45.464841, 9.188804),
      zoom              : 3,
      styles            : this.mapStyle,
      backgroundColor   : '#1f1f1f',
      zoomControl       : false,
      streetViewControl : false,
      scaleControl      : false,
      panControl        : false,
      mapTypeControl    : false

    };

    this.cities   = [];
    this.markers  = [];

    this.images       = [];
    this.imagesLoaded = 0;
    this.thumbs       = $('.slider-el picture');
    this.elements     = $('.slider-el');
    this.filters      = $('.authors-filters li');
    this.mapFilters   = this.self.find('.authors-map-filters a');

    this.initMap();
    this.fetchData();
    this.placeMarkers();
    this.initThumbs();
    this.initFilters();


    var self = this;

    this.infobox.find('.icn-close').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.hideInfobox();
      self.infoboxVisible = false;
    });

    // mobile infobox
    this.infoboxMobile =  this.self.find('.authors-map_infobox-mobile');
    this.infoboxMobile.find('.icn-close').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.infoboxMobile.removeClass('authors-map_infobox-mobile__show');
      self.infoboxVisible = false;
      setTimeout(function(){
        self.infoboxMobile.hide();
      },200);
    });
    this.infoboxMobileContent = this.infoboxMobile.find('>div');

    // bind on map controls
    this.self.find('.authors-toggler').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.togglePanel();
    });
    $('.slider__authors-close').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.togglePanel();
    });

    this.self.find('.authors-map_zoom-in').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.map.setZoom(self.map.getZoom()+1);
    });
    this.self.find('.authors-map_zoom-out').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.map.setZoom(self.map.getZoom()-1);
    });
    this.mapFilters.on(app.startEvent,function(){
      var lat   = parseFloat(this.getAttribute('data-lat')),
          lng   = parseFloat(this.getAttribute('data-lng')),
          zoom  = parseInt(this.getAttribute('data-zoom'));
      self.mapFilters.removeClass('authors-map-filters_active');
      this.classList.add('authors-map-filters_active');

      if( lat === 0 && lng === 0 ){
        self.map.fitBounds(self.bounds);
        return;
      }
      self.map.setZoom(zoom);
      self.map.panTo(new google.maps.LatLng(lat,lng));
    });

    window.setTimeout(function(){
      self.slider.css('display','block');
    },1000);
  }

  AuthorsMap.prototype.initMap = function(){
    var self    = this;
    this.map    = new google.maps.Map(this.canvas[0],this.mapOptions);

    // hide infobox on map events
    google.maps.event.addListener(this.map,'zoom_changed',function(){ self.hideInfobox(); });
    //google.maps.event.addListener(this.map,'click',function(){ self.hideInfobox(); });
    google.maps.event.addListener(this.map,'center_changed',function(){ self.hideInfobox(); });

  };

  AuthorsMap.prototype.fetchData = function(){
    var data    = this.self.find('.authors-data'),
        city,
        lat,
        lng,
        coords;
    this.bounds  = new google.maps.LatLngBounds();
    for(var i=data.length-1; i>-1;i--){
      city    = data[i].querySelector('.authors-data_city');
      lat     = parseFloat(city.getAttribute('data-lat'));
      lng     = parseFloat(city.getAttribute('data-lng'));
      coords  = new google.maps.LatLng(lat,lng);
      this.cities.push({
        name    : city.textContent,
        authors : data[i].querySelectorAll('a'),
        latLng  : coords
      });

      this.bounds.extend(coords);
    }

    //this.map.fitBounds(this.bounds);
  };

  AuthorsMap.prototype.placeMarkers = function(){
    var marker;
    for(var i=this.cities.length-1;i>-1;i--){
      marker = new AuthorMapMarker(i,this.map,new google.maps.Marker({
        position  : this.cities[i].latLng,
        title     : this.cities[i].name
      }),this,this.showAuthorsOnMap);

      this.markers.push(marker);

    }

  };

  AuthorsMap.prototype.showAuthorsOnMap = function(id,position,currentMarker){
    var singleElementHeight  = 40,
        offsetLeft          = parseInt( this.canvas.css('left')) + 20,
        offsetTop,
        items               = Math.min(this.cities[id].authors.length , 5);

    offsetTop = parseInt(this.canvas.css('top')) - (singleElementHeight * items / 2);

    var self = this;

    if( this.currentMarker ) {
      this.currentMarker.setOff();
    }

    this.currentMarker = currentMarker;

    this.currentMarker.setOn();
    this.infobox.css({ top:position.y+offsetTop , left:position.x+offsetLeft });
    this.infoboxTitle.html(this.cities[id].name);
    this.infoboxContent.html(this.cities[id].authors);

    this.infobox.show();
    self.infoboxVisible = true;

    if( w.innerWidth <= 480 ){
      var fragment  = d.createDocumentFragment(),
          authors   = this.cities[id].authors;
      for(var i=0; i<authors.length;i++){
        fragment.appendChild(authors[i].cloneNode(true));
      }

      this.infoboxMobile.find('h6').html(this.cities[id].name);
      this.infoboxMobileContent.html(fragment);
      this.infoboxMobile.show();

      setTimeout(function(){
        self.infoboxMobile.addClass('authors-map_infobox-mobile__show');
      },200);

    }
  };

  AuthorsMap.prototype.togglePanel = function(){
    if( this.panelVisible ){
      this.panelVisible = false;
      this.panelOverlay.fadeOut(300);
      this.content.removeClass('authors-panel__show');
    }else{
      this.panelVisible = true;
      this.panelOverlay.fadeIn(300);
      this.content.addClass('authors-panel__show');
    }
    this.hideInfobox();
  };

  AuthorsMap.prototype.initThumbs = function(){
    var self = this;
    this.thumbs.each(function(){
      self.images.push( new ImageLazy(this,self,self.imageLoaded) );
    });
  };

  AuthorsMap.prototype.imageLoaded = function(){
    this.imagesLoaded += 1;
    if( this.imagesLoaded === 2 ){
      app.$w.trigger('pageLoadingEnd');
    }

    if( this.imagesLoaded === (this.thumbs.length -1) ){
      this.images = null;
    }
  };

  AuthorsMap.prototype.initFilters = function(){
    var self = this;

    // fix scrollbar problem
    if( app.scrollbarWidth > 0 ) {
      var elements  = this.slider.find('.slider-el'),
          elWidth   = elements.width();
      elements.css('width',elWidth-(app.scrollbarWidth/2));
    }

    this.filters.on(app.startEvent,function(){
      self.filters.removeClass('authors-filters_active');
      var filter = this.getAttribute('data-filter');
      self.filterBy(filter);
      this.classList.add('authors-filters_active');
    });
  };

  AuthorsMap.prototype.filterBy = function(id){
    // hide slider
    var self = this;
    this.slider.addClass('authors-filters__hide');


    window.setTimeout(function(){
      // filter elements
      self.elements.removeClass('authors-filters__disabled');
      var l           = self.elements.length;

      // if the filter selected
      if( id === 'all' ){
        self.slider.removeClass('authors-filters__hide');
        return;
      }

      /*for(var i=0;i<l;i++){
        if( self.elements[i].getAttribute('data-filter').contains(id) ){
          filtered.push(self.elements[i]);
        }else{
          self.elements[i].classList.add('authors-filters__disabled');
          disabled.push(self.elements[i]);
        }
      }

      self.sliderContent.html(filtered.concat(disabled));*/
      for(var i=0;i<l;i++) {
        if (self.elements[i].getAttribute('data-filter').contains(id)) {
         self.elements[i].classList.remove('authors-filters__disabled');
        }else{
          self.elements[i].classList.add('authors-filters__disabled');
        }
      }


      self.overables = new OverableElements('.slider-el','authors-filters__disabled');
      self.slider.removeClass('authors-filters__hide');

    },800);

  };

  AuthorsMap.prototype.hideInfobox = function(){
    this.infobox.hide();
    if( this.currentMarker ){
      this.currentMarker.setOff();
      this.currentMarker = null;
    }
  };


  /**
   * General leaf page
   *
   * @class Single
   * @constructor
   */
  function Single(){
    this.self         = $('.project');
    this.thumbs       = this.self.find('.project-el picture');
    this.imagesLoaded = 0;
    this.images       = [];
    this.thumbsFull   = [];
    this.lightbox     = new Lightbox();
    this.minImgLoaded = Math.min(2,this.thumbs.length);

    for(var i=0; i<this.thumbs.length;i++){
      this.images.push(new ImageLazy(this.thumbs[i],this,this.imageLoaded));
    }

    var self      = this,
        elements  = this.self.find('.project-el');

    elements.each(function(i){
      this.setAttribute('data-project-index',i);
      self.thumbsFull.push({
          href  : this.getAttribute('data-project-full'),
          title : this.getAttribute('data-project-title')
      });
    });

    elements.on('click',function(){
      var ordered = self.thumbsFull,
          index   = this.getAttribute('data-project-index');
      ordered = ordered.slice(index).concat(ordered.slice(0,index));
      self.lightbox.show(ordered);
    });

  }

  Single.prototype.setupImages = function(){
    if( this.thumbs.length === 1 ){
      this.self.addClass('project__one-element');
    }

  };

  Single.prototype.imageLoaded = function(){
    this.imagesLoaded += 1;
    if( this.imagesLoaded === this.minImgLoaded ){
      app.$w.trigger('pageLoadingEnd');
    }

    if( this.imagesLoaded === (this.thumbs.length) ){
      this.setupImages();
      this.images = null;
    }
  };

  /**
   * Custom select element
   *
   * @class Select
   * @constructor
   */
  function Select(el){
    this.self       = $(el);
    this.label      = this.self.find('.contact-select_value');
    this.dropdown   = this.self.find('.contact-select_dropdown');
    this.options    = this.self.find('select option');

    // on mobile use native select
    if( Modernizr.touch ){
      var self = this;
      this.selectElement(0);
      this.self.find('select').on('blur',function(){
        var current;
        for(var i=0;i<self.options.length;i++){
          if( self.options[i].selected === true ){
            current = self.options[i];
            self.selectElement(current.getAttribute('data-index'));
          }
        }
      });
    }

    this.buildDropdown();
  }

  Select.prototype.buildDropdown = function(){
    var html    = '<ul>',
        current,
        self    = this;
    for(var i=0; i<this.options.length;i++){
      current = this.options[i];
      current.setAttribute('data-index', i.toString());

      html += '<li data-index="'+i+'">'+this.renderElement(current)+'</li>';
    }

    html += '</ul>';

    this.dropdown.html(html);
    this.selectElement(0);

    this.dropdown.find('li').on(app.startEvent,function(){
      self.selectElement(this.getAttribute('data-index'));
    });

    this.self.on(app.startEvent,function(){
      self.dropdown.hide();
      self.self.removeClass('select__focus');
    });
    this.label.on(app.startEvent,function(evt){
      evt.stopPropagation();
      self.dropdown.toggle();
      self.self.toggleClass('select__focus');
    });

    this.self.find('.icn-disclosure').on(app.startEvent,function(evt){
      evt.stopPropagation();
      self.dropdown.toggle();
      self.self.toggleClass('select__focus');
    });

  };

  Select.prototype.renderElement = function(el){
    var components  = el.textContent.split('|');
    return components[0]+'<span>'+components[1]+'</span>';
  };

  Select.prototype.selectElement = function(id){
    var current = this.options[id];
    Array.prototype.map.call(this.options,function(el){ el.removeAttribute('selected'); });
    current.setAttribute('selected','selected');
    this.label.html(this.renderElement(current));
    this.dropdown.hide();
  };


  /**
   * General layer class
   *
   * @class Layer
   * @constructor
   */
  function Layer(id,title,trigger,inPageClass,obj,readyFn){
    this.trigger      = $(trigger);
    this.isPage       = ($(inPageClass).length>0);
    this.url          = this.trigger.attr('href');
    this.caller       = obj;
    this.readyFn      = readyFn;
    this.maxVieport   = 1024;
    this.initialized  = false;
    this.pageId       = id;
    this.title        = title;
    this.visible      = false;
    this.overlay      = null;
    this.navLinks     = $('.main-nav a');

    if( this.isPage ){
      this.readyFn.call(this.caller);
    }

    var self = this;

    if(w.innerWidth>=this.maxVieport ){
      this.trigger.on(app.startEvent,function(evt){
        evt.preventDefault();
        var state = History.getState();
        if( state.url.contains(self.url) ){
          return;
        }
        self._init();
      });
    }
  }

  Layer.prototype._init = function() {
    var self = this;

    if (!this.initialized) {
      // load layer
      this.self     = $('<div class="layer"></div>').appendTo('body');
      this.overlay  = $('<div class="layer-overlay"></div>').appendTo('body');
      this.self.load(this.url+' #'+this.pageId, function () {

        // init layer
        self.width = self.self.width();

        // bind events
        self.close = $('<a class="layer-close" href="#"><i class="icn icn-back"></i></a>').prependTo(self.self.find('.page-title'));
        self.close.on(app.startEvent, function (evt) {
          evt.preventDefault();
          History.go(-1);
          self.toggleLayer(false);
        });

        /*History.Adapter.bind(window, 'statechange', function () {
          var state = History.getState();
          if (!state.url.contains(self.pageId)) {
            self.toggleLayer(false);
          } else {
            self.toggleLayer(true);
          }
        });*/

        self.readyFn.call(self.caller,true);

        self.initialized = true;
        self.toggleLayer(true);

      });
    } else {
      this.toggleLayer(true);
    }
  };

  Layer.prototype.toggleLayer = function(direction){
    if(direction){

      // close other opened layer if any
      if( app.currentLayer ){
        app.currentLayer.toggleLayer(false);
        History.go(-1);
      }
      // register as current layer
      app.currentLayer = this;
      History.pushState({state:this.pageId},this.title,this.url);
      this.self.addClass('layer__visible');
      this.overlay.fadeIn();
      this.visible = true;
      app.blockScroll = true;
      this.navLinks.removeClass('nav-active');
      this.trigger.addClass('nav-active');
    }else{
      this.self.removeClass('layer__visible');
      this.overlay.fadeOut();
      this.visible = false;
      app.blockScroll   = false;
      app.currentLayer  = null;
      this.trigger.removeClass('nav-active');

      // re-add active class to the real element
      app.currentLink.addClass('nav-active');

    }
  };

  /**
   * Insight page
   *
   * @class Insight
   * @constructor
   */
  function Insight(){
    this.self                 = $('.insight');
    this.thumbs               = this.self.find('.img-lazyload ');
    this.textContainer        = this.self.find('.insight-info');
    this.teamContainer        = this.self.find('.insight-team');
    this.loadCounter          = 0;
    this.images               = [];
    var self = this;
    this.thumbs.each(function(i,e){
      self.images.push( new ImageLazy(e,self,self.imagesLoaded) );
    });

    new OverableElements('.insight-team ul li','noclass');

    app.resize(this);
    this.update();
  }

  Insight.prototype.imagesLoaded = function(){
    this.loadCounter +=1 ;
    if( this.loadCounter === 3 ){
      app.$w.trigger('pageLoadingEnd');
    }
  };

  Insight.prototype.update = function(){
    if( window.innerWidth > 768 ) {
      this.teamContainer.css('height', w.innerHeight);
      this.textContainer.css('height', w.innerHeight);
    }else{
      this.teamContainer.css('height', 'auto');
      this.textContainer.css('height', 'auto');
    }
  };

  /**
   * Clients page
   *
   * @class Clients
   * @constructor
   */
  function Clients(){
    this.layer = new Layer('clients','Clients','.clients-link','.clients-page',this,this._init);
  }

  Clients.prototype._init = function(){
    this.tabs = new Tabs();
  };


  /**
   * Contact form/page
   *
   * @class Contact
   * @constructor
   */
  function Contact(){

    this.layer          = new Layer('contact','Contacts','.contact-link','.contact-page',this,this._init);
    this.initialized    = false;
    this.rexp           = {
      email       : /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/,
      required    : /[0-9a-zA-Z]+/
    };

  }

  Contact.prototype._init = function(){
    var self      = this;
    this.self     = $('.contact-form');
    this.select   = new Select(this.self.find('.contact-select'));
    this.inputs   = this.self.find('.contact-field input,.contact-field textarea');
    this.textarea = this.self.find('.contact-field textarea');

    // just for resizing purpose
    var fields  = this.self.find('.contact-field');
    var margin  = parseInt(fields.eq(0).css('margin-bottom'));
    this.contactsSize = ((fields.eq(0).height()+margin)*fields.length) + (3*margin) + this.self.find('.contact-controls').height() + 43; // 43 = padding

    this.form         = this.self.find('.contact-field,.contact-controls');
    this.messageError = this.self.find('.contact-response_ko');
    this.messageDone  = this.self.find('.contact-response_ok');

    app.resize(this);
    this.update();

    this.self.find('.contact-submit').on(app.startEvent,function(evt){
      evt.preventDefault();
      self.validate();
    });

  };

  Contact.prototype.validate = function(){
    var valid = true,validator,current;
    for(var i=0;i<this.inputs.length;i++){
      current   = this.inputs[i];
      validator = this.rexp[current.getAttribute('data-valid')];
      if( validator.test(current.value) ){
        current.classList.remove('contact-error');
      }else{
        current.classList.add('contact-error');
        valid = false;
      }
    }

    if( valid ){
      // ajax request
      var data  = {},
          url   = this.self.attr('action'),
          self  = this;
      this.self.find('input[type="text"],select,textarea').each(function(){
        data[this.getAttribute('name')] = this.value;
      });

      this.form.fadeOut();

      $.ajax({
        type	: 'POST',
        url		: url,
        dataType: 'JSON',
        data	: data,
        context	: document.body
      }).done(function( result ) {
       if( result.response === 'ok' ){
         self.self[0].reset();
         self.messageDone.fadeIn().delay(6000).fadeOut();
         self.form.delay(7000).fadeIn();
       }else{
         self.messageError.fadeIn().delay(6000).fadeOut();
         self.form.delay(7000).fadeIn();
       }

      });

    }

  };

  Contact.prototype.update = function(){
    if(w.innerWidth >= 768 ) {
      this.textarea.css('height', ( w.innerHeight - this.contactsSize ));
    }
  };

  /**
   * Lightbox (fancybox wrapper)
   *
   * @class Lightbox
   * @constructor
   */
  function Lightbox(){
    var self = this;

    this.initialized    = false;
    this.fancyboxConfig = {
      autoSize    : true,
      autoResize  : true,
      nextSpeed   : 250,
      preload     : 4,
      nextEffect  : 'fade',
      prevEffect  : 'fade',
      helpers     : {
        media   : true,
        overlay : { locked : false }
      },
      youtube : { autoplay : 0 },
      vimeo   : { autoplay : 0 },
      wrapCSS     : 'fancybox-wrapper',
      counter     : false,
      arrows      : false,
      closeBtn    : false,
      closeClick  : false,
      mouseWheel  : false,
      nextClick   : false,
      afterShow   : function(){
        self.showHeader();
        if( $.fancybox.current.group.length === 1 ){
          self.controls.addClass('lightbox-controls__noarrows');
        }
        self.controls.addClass('lightbox-controls__show');

      },
      beforeClose : function(){
        self.hideHeader();
        self.controls.removeClass('lightbox-controls__show');
        setTimeout(function(){
          self.controls.css('display','none');
        },500);
      }
    };

  }

  Lightbox.prototype._init = function(){
    this.controls       = $('<div class="lightbox-controls"></div>');
    this.prev           = $('<a class="lightbox-control lightbox-prev"><i><svg><use xlink:href="#icon-prev"></use></svg></i></a>').appendTo(this.controls);
    this.next           = $('<a class="lightbox-control lightbox-next"><i><svg><use xlink:href="#icon-next"></use></svg></i></a>').appendTo(this.controls);
    this.header         = $('<div class="lightbox-header"><span class="lightbox-counter"></span><p></p></div>').appendTo(this.controls);
    this.headerContent  = this.header.find('>p');
    this.counter        = this.header.find('.lightbox-counter');
    this.close          = $('<div class="lightbox-control lightbox-close"><i class="icn-close"></i></div>').appendTo(this.controls);

    var self = this;

    this.next.on(app.startEvent,function(){
      $.fancybox.next();
    });

    this.prev.on(app.startEvent,function(){
      $.fancybox.prev();
    });

    this.close.on(app.startEvent,function(){
      self.controls.removeClass('lightbox-controls__show');
      self.controls.removeClass('lightbox-controls__noarrows');
      setTimeout(function(){
        self.controls.css('display','none');
      },500);
      $.fancybox.close();
    });

    this.controls.appendTo('body');
  };

  Lightbox.prototype.show = function(media){

    // do not show lightbox on small viewport
    if( w.innerWidth <= 768 ){
      return;
    }

    if( ! this.initialized ){
      this._init();
      this.initialized = true;
    }
    this.controls.css('display','block');

    $.fancybox.open(media,this.fancyboxConfig);

  };

  Lightbox.prototype.showHeader = function(){
    var title = $.fancybox.current.title;
    if( !title ){
      return;
    }
    this.header.css('display','block');
    this.headerContent.html(title);
    this.updateCounter();
    if( !this.header.hasClass('lightbox-header__show') ){
      this.header.addClass('lightbox-header__show');
    }
  };

  Lightbox.prototype.hideHeader = function(){
    this.header.removeClass('lightbox-header__show');
  };

  Lightbox.prototype.updateCounter = function(){
    var current = $.fancybox.current;
    this.counter.text((current.index+1)+'/'+current.group.length);
  };

  /**
   * FeatureSingle (single feature media object)
   *
   * @class FeatureSingle
   * @param el {Object}
   * @param parent {Object} object which will be notified when the component is ready
   * @param fn {Function} ready callback
   * @constructor
   */
  function FeatureSingle(el,parent,fn){
    this.self               = $(el);
    this.image              = this.self.find('picture')[0];
    this.imageObj           = new ImageLazy(this.image,this,this.imageLoaded);
    this.parent             = parent;
    this.fn                 = fn;
    this.textWrapper        = this.self.find('.feature-item-text');
    this.textWrapperContent = this.textWrapper.find('>div');
    this.text               = this.textWrapperContent.text();
    this.pageIndex          = 0;
    this.paginateText();

    //app.resize(this);
  }

  FeatureSingle.prototype.imageLoaded = function(){
    this.fn.call(this.parent);
  };

  FeatureSingle.prototype.paginateText = function(){
    var text    = this.text,
        l       = text.length,
        max     = 0,
        parts   = '',
        pages,
        html,
        self,
        i,
        winW;

    winW = window.innerWidth;
    if( winW>=1280 ){
      max = 600;
    }else if( winW>1024 ){
      max = 500;
    }else{
      max = 400;
    }

    if( l<=max ){
      return;
    }

    // split text
    pages = Math.ceil(l/max);
    for(i=0;i<pages;i++){
      parts+= '<div class="feature-item_page">'+text.substring(i*max,(i*max)+max)+'</div>';
    }
    this.textWrapperContent.html(parts);
    this.pages = this.textWrapperContent.find('.feature-item_page');

    // build pager
    html = '<ul class="feature-item_pager">';
    for(i=0;i<pages;i++){
      html += '<li>'+i+'</li>';
    }
    html += '</ul>';

    self = this;
    this.pager = $(html).appendTo(this.textWrapper);
    this.pager.on(app.startEvent,'li',function(){
      var i = parseInt(this.textContent);
      self.pager.find('li').removeClass('active');
      this.classList.add('active');
      self.pages.hide();
      self.pages.eq(i).show();
      self.pageIndex = i;
    });

    this.pages.eq(0).show();
    this.pager.find('li:first-child').addClass('active');

    if( Modernizr.touch ){
      var hammertime = new Hammer(this.self.find('.feature-item-text>div')[0],{preventDefault: true});
      hammertime.on('swipeleft',function(){
        var i = ( self.pageIndex > -1 )? self.pageIndex +1 : 0;
        self.pager.find('li').removeClass('active');
        self.pager.find('li').eq(i).addClass('active');
        self.pages.hide();
        self.pages.eq(i).show();
        self.pageIndex = i;
      });
      hammertime.on('swiperight',function(){
        var i = ( self.pageIndex < self.pages.length )? self.pageIndex -1 : self.pageIndex;
        self.pager.find('li').removeClass('active');
        self.pager.find('li').eq(i).addClass('active');
        self.pages.hide();
        self.pages.eq(i).show();
        self.pageIndex = i;
      });
    }

  };

  FeatureSingle.prototype.update = function(){
    this.paginateText();
  };


  /**
   * FeatureText (feature text)
   *
   * @class FeatureText
   *
   */
  function FeatureText(){
    this.self         = $('.feature');
    this.featureText  = this.self.find('.feature-text');
    this.header       = this.self.find('.feature-header');
    this.headerOffset = parseInt(this.header.css('padding-top'));

    var self = this;

    if( Modernizr.touch ){
      return;
    }
    this.featureText.on('scroll',function(){
      var scrollValue = this.scrollTop;
      self.setHeader();
      if( scrollValue >= self.headerOffset ){
         self.header.addClass('feature-fixed');
      }else{
        self.header.removeClass('feature-fixed');
      }

    });
  }


  FeatureText.prototype.setHeader = function(){
    this.header.css('width',this.featureText.width());
  };

  FeatureText.prototype.update = function(){
    if(w.innerWidth>1023 ) {
      this.featureText.css('height', w.innerHeight);
      this.setHeader();
    }
  };

  /**
   * Feature
   *
   * @class Feature
   *
   */

  function Feature(){
    this.self           = $('.feature');
    this.text           = new FeatureText();
    this.media          = [];
    this.itemsWrapper   = this.self.find('.feature-elements');
    this.items          = this.self.find('.feature-item').length;
    this.itemsLoaded    = 0;
    this.minimumLoad    = 1;
    this.thumbs         = this.self.find('picture');
    this.close          = this.self.find('.feature-close');

    var self = this;
    this.self.find('.feature-item').each(function(){
      self.media.push(new FeatureSingle(this,self,self.imageLoaded));
    });

    if( this.self.hasClass('feature-video') ){
      var media = this.self.find('.feature-media');
      this.lightbox = new Lightbox();
      media.on('click',function(){
        self.lightbox.show( { href : this.getAttribute('data-video') } );
      });
    }else{
      this.thumbs.on('click','img',function(){
        self.self.toggleClass('feature_showItemsText');
      });
    }

    this.close.on(app.startEvent,function(evt){
      evt.preventDefault();
      self.self.removeClass('feature_showItemsText');
    });

    app.resize(this);
    this.update();
    this.setupImages();
  }

  Feature.prototype.update = function(){

    if(w.innerWidth<1024 ){
      return;
    }

    this.text.update();
    this.itemsWrapper.css('height', w.innerHeight);
  };

  Feature.prototype.imageLoaded = function(){
    this.itemsLoaded++;
    if( this.itemsLoaded === this.minimumLoad ){
      app.$w.trigger('pageLoadingEnd');
    }
  };

  Feature.prototype.setupImages = function(){
    if( this.thumbs.length === 1 ){
      this.self.addClass('feature__one-element');
    }
  };



  function Tabs(){
      this.self       = $('.tabs');
      this.controls   = this.self.find('.tabs-controls');
      this.contents   = this.self.find('.tabs-content');
      this.mapContents = {};
      this.mapControls = {};
      this.startTab    = this.controls.data('tabs-start');

    var self = this;
    this.contents.each(function(){
      self.mapContents[this.getAttribute('data-tabs')] = $(this);
      this.classList.add('cf');
    });

    this.controls.children().each(function(){
      self.mapControls[this.getAttribute('data-tabs-target')] = $(this);
    });


    this.controls.on( 'click' , 'li' , function(evt){
      var id = evt.target.getAttribute('data-tabs-target');
      self.showTab(id);
    });


    this.showTab(this.startTab);

  }

  Tabs.prototype.showTab = function(id){
    this.contents.hide();
    this.controls.children().removeClass('tabs-current');

    this.mapContents[id].show();
    this.mapControls[id].addClass('tabs-current');
  };


  /*=== Application main ===*/
  var app = {
    version           : '1.0',
    images            : [],
    resizeSubscribers : [],
    resizeTimer       : null,
    $w                : $(w),
    $d                : $(d),
    page              : $('#content').attr('data-page'),
    platform          : (Modernizr.touch) ? 'mobile'      : 'desktop',
    startEvent        : (Modernizr.touch) ? 'touchstart'  : 'click'
  };


  var baseurl   = d.body.getAttribute('data-baseurl'),
      location  = window.location,
      url       = location.protocol+'//'+location.host;
  app.resURL    = (baseurl)? url + d.body.getAttribute('data-baseurl') : '/';
  app.URL       = location.protocol+'//'+location.pathname;

  app.currentLink = $('.nav-active');

  app.resize = function(obj){
    app.resizeSubscribers.push(obj);
  };

  app.prefixedProperty = function(prop){
    return Modernizr.prefixed(prop).replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');
  };

  // iPad class
  if (navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
    $('html').addClass('ipad-ios7');
  }

  // debouncing resize events
  app.$w.resize(function(){
    if(!app.resizeTimer){
      app.resizeSubscribers.forEach(function(el){
        el.update();
      });
      app.resizeTimer = setTimeout(function(){
        app.resizeTimer = null;
      },100);
    }
  });

  // orientation change
  if( Modernizr.touch ) {
    /*w.addEventListener('orientationchange', function () {
      app.resizeSubscribers.forEach(function (el) {
        el.update();
      });
    });*/
  }

  // delegate
  app.delegate = function(obj,fn){
    return (function(obj,fn,arg){
      return function(){ fn.apply(obj, arg);};
    })(obj,fn,Array.prototype.slice.call(arguments,2));
  };

  // block scroll
  app.blockScroll = false;

  // detect scrollbar width
  var div = d.createElement('div');
  div.className = 'scrollbar-tester';
  document.body.appendChild(div);
  app.scrollbarWidth = div.offsetWidth - div.clientWidth;
  document.body.removeChild(div);

  // init default components
  app.loader        = new Loader();
  app.navigation    = new Navigation();
  app.contacts      = new Contact();
  app.clients       = new Clients();
  app.currentLayer  = null;

  // specific components
  if( app.page === 'homepage' ){
    app.homeslider = new HpSlider();
  }

  if( app.page === 'list' ){
    app.filters = new Filters();
    app.slider  = new Slider();
  }

  if( app.page === 'author-list' ){
    app.authormap = new AuthorsMap();
  }

  if( app.page === 'author-profile' ){
    app.author  = new Author();
  }

  if( app.page === 'single' ){
    app.single  = new Single();
  }

  if( app.page === 'contact-page' || app.page === 'why-page' || app.page === 'clients-page' ){
    app.$w.trigger('pageLoadingEnd');
  }

  if( app.page === 'features' ){
    app.features = new Feature();
  }

  if( app.page === 'insight'){
    app.insight = new Insight();
  }



}(window,document,undefined));
